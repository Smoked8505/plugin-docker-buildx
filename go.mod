module codeberg.org/woodpecker-plugins/plugin-docker-buildx

go 1.18

require (
	codeberg.org/6543/go-yaml2json v0.3.0
	github.com/coreos/go-semver v0.3.0
	github.com/drone-plugins/drone-plugin-lib v0.4.0
	github.com/joho/godotenv v1.4.0
	github.com/sirupsen/logrus v1.9.0
	github.com/stretchr/testify v1.8.0
	github.com/urfave/cli/v2 v2.16.3
	golang.org/x/lint v0.0.0-20210508222113-6edffad5e616
	honnef.co/go/tools v0.3.3
)

require (
	github.com/BurntSushi/toml v1.1.0 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
	golang.org/x/exp/typeparams v0.0.0-20220218215828-6cf2b201936e // indirect
	golang.org/x/mod v0.6.0-dev.0.20220419223038-86c51ed26bb4 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
	golang.org/x/tools v0.1.11-0.20220513221640-090b14e8501f // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
