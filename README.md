# plugin-docker-buildx

<a href="https://codeberg.org/woodpecker-plugins/plugin-docker-buildx">
    <img alt="Get it on Codeberg" src="https://codeberg.org/Codeberg/GetItOnCodeberg/media/branch/main/get-it-on-neon-blue.png" height="60">
</a>

Woodpecker CI plugin to build multiarch Docker images with buildx

Woodpecker CI plugin to build multiarch Docker images with buildx. This plugin is a fork of [thegeeklab/drone-docker-buildx](https://github.com/thegeeklab/drone-docker-buildx/) which itself is a fork of [drone-plugins/drone-docker](https://github.com/drone-plugins/drone-docker). You can find the full documentation at You can find the full documentation at [woodpecker-ci.org](https://woodpecker-ci.org/plugins/Docker%20Buildx).

## Contributors

Special thanks goes to all [contributors](https://codeberg.org/woodpecker-plugins/plugin-docker-buildx/activity). If you would like to contribute,
please see the [instructions](https://codeberg.org/woodpecker-plugins/plugin-docker-buildx/src/branch/main/CONTRIBUTING.md).

## License

This project is licensed under the Apache-2.0 License - see the [LICENSE](https://codeberg.org/woodpecker-plugins/plugin-docker-buildx/src/branch/main/LICENSE) file for details.
